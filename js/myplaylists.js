var playlists;

$(document).ready(function() {
    playlists = JSON.parse(localStorage.getItem('helloEchoPlaylists'));

    if (!playlists || playlists.length === 0) {
      displayError('It looks like you don\'t have any playlists yet. Go to the home page and create one!')
    } else {
      buildPlaylistsHTML();
    }
});

function buildPlaylistsHTML() {
    html_string = "";

    $.each(playlists, function(index, playlist) {
        var html_start = '<div class="panel panel-default"><div class="panel-body playlist-items">';
        var html_end = '</div></div>';
        var playlist_name = '<a href="playlist.html?i=' + index + '"><b>' + playlist.name + ' </b></a>';
        playlist_name += '<a class="glyph" href="javascript:editPlaylistModal(' + index + ')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
        playlist_name += '<a class="glyph" href="javascript:deletePlaylistModal(' + index + ')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a>';
        var sample_tracks = "";
        var i = 0;
        while (sample_tracks.length < 120) {
            if (i < playlist.songs.length) {
                sample_tracks += playlist.songs[i].artists[0].name + ' - <i>' + playlist.songs[i].name + '</i>, ';
                i++;
            } else {
                break;
            }
        }
        sample_tracks = '<div class="sample-tracks">' + sample_tracks + '...</div>';
        var new_html = html_start + playlist_name + sample_tracks + html_end;
        html_string = html_string + new_html;
    });

    $("#error").html('');
    $('#playlists').html(html_string);
}

function displayError(errorMessage) {
    $("#playlists").html('');
    $('#error').html('<div class="alert alert-danger" role="alert">' + errorMessage + '</div>');
}

function editPlaylistModal(i) {
    var confirm_button = '<button class="btn btn-success" onclick="javascript:changePlaylistName(' + i + ')">Save</button>';
    $('#confirm-btn').html(confirm_button);

    var change_playlist_name_form = '<input id="edit-playlist-modal-text" type="text" class="form-control" placeholder="New Playlist Name" autofocus>';

    $('#edit-playlist-modal-title').html('Rename "' + playlists[i].name + '"');
    $('#edit-playlist-modal-body').html(change_playlist_name_form);
    $('#save-playlist-name-btn').attr('onclick', 'javascript:changePlaylistName("' + i + '")');

    $('#edit-playlist-modal').modal();
}

function changePlaylistName(i) {
    var newPlaylistName = $('#edit-playlist-modal-text').val();
    if (newPlaylistName === '') {
        $('#edit-playlist-modal').modal('hide');
        return;
    }
    playlists[i].name = newPlaylistName;
    localStorage.setItem("helloEchoPlaylists", JSON.stringify(playlists));
    $('#edit-playlist-modal').modal('hide');
    buildPlaylistsHTML();
}

function deletePlaylistModal(i) {
    $('#delete-btn').html('');
    $('#edit-playlist-modal-title').html('Confirm Delete');
    $('#edit-playlist-modal-body').html('Are you sure you want to delete the playlist "' + playlists[i].name + '"?');
    $('#confirm-btn').html('<button id="save-playlist-name-btn" type="button" class="btn btn-danger" onclick="javascript:deletePlaylist(' + i + ')">Confirm Delete</button>');

    $('#edit-playlist-modal').modal();
}

function deletePlaylist(i) {
    playlists.splice(i, 1);
    localStorage.setItem("helloEchoPlaylists", JSON.stringify(playlists));
    $('#edit-playlist-modal').modal('hide');
    buildPlaylistsHTML();
    updateBadgeCount(playlists.length);
}
