
var access_token = "BQAz5UmA5DVBVuqsSjKWZ--EYRooP0KXkQs4V9CUKtIWNF67hT12POkVmc6_dTbotUDbYEJgSKhnBW4rPOxvG9g420m5lgB1LRXKJP0oX7hRNr_uUxnMABwdKuin_-2ajPKyrw";

var maxHotness = 1.0;
var minHotness = 0.5;
var maxYear = 2015;
var minYear = 2010;
var maxDance = 1.0;
var minDance = 0.0;

var genre;

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
  $('[data-toggle="popover"]').popover();

  $("#show_advanced_search").click(function(){
	$("#simple-search").hide();
    $("#advanced_search").fadeIn();
  });

  $("#hide_advanced_search").click(function(){
	$("#simple-search").fadeIn();
    $("#advanced_search").hide();
  });

  $(document).ready(function(){
    $("#down_arrow").localScroll({duration:600});
  });

  $("#show_playlist_options").click(function(){
    $("#playlist_options_form").fadeIn();
  });

  $(".dropdown-menu li a").click(function(){
    $(this).parents(".dropdown").find('.btn').html($(this).text() + ' <span class="caret"></span>');
    $(this).parents(".dropdown").find('.btn').val($(this).data('value'));
  })

  $("#x").click(function(){
    $("#playlist_options_form").fadeOut();
  });

  $(function() {
      $('#advanced_search').each(function() {
          $(this).find('input').keypress(function(e) {
              if(e.which == 10 || e.which == 13) {
                  advancedSearch();
              }
          });
      });
  });

  $(function(){
	var hotnessLow;
	var hotnessHigh;
	$("#hotness_slider").slider({
		range: true,
		min: 0,
		max: 100,
		values: [50,100],
		slide: function(event,ui){
			$("#hotness_level").val(ui.values[0] + " - " + ui.values[1]);
      minHotness = ui.values[0]/100.0;
      maxHotness = ui.values[1]/100.0;
		}
	  });
    $("#hotness_level").val($("#hotness_slider").slider("values",0) + " - " + $("#hotness_slider").slider("values",1));
  });

  $(function(){
  $("#dance_slider").slider({
    range: true,
    min: 0,
    max: 100,
    values: [0,100],
    slide: function(event,ui){
      $("#dance_range").val(ui.values[0] + " - " + ui.values[1]);
      minDance = ui.values[0]/100.0;
      maxDance = ui.values[1]/100.0;
    }
    });
    $("#dance_range").val($("#dance_slider").slider("values",0) + " - " + $("#dance_slider").slider("values",1));
  });

  $(function(){
	$("#year_slider").slider({
		range: true,
		min: 1950,
		max: 2015,
		values: [1990,2015],
		slide: function(event,ui){
			$("#year_range").val(ui.values[0] + " - " + ui.values[1]);
      minYear = ui.values[0];
      maxYear = ui.values[1];
		}
	});
	$("#year_range").val($("#year_slider").slider("values",0)+ " - " + $("#year_slider").slider("values",1));
  });
});

$(window).scroll(function() {
    if ($(this).scrollTop() > 200){ // Set position from top to add class
       $('#navBar').addClass("shrink");
     } else {
        $('#navbar').removeClass("shrink");
    }
 });
 $(function () {
   $(document).on('scroll', function () {
  if ($(this).scrollTop() > 200){ // Set position from top to add class
        $('#navBar').addClass("shrink");
    } else {
        $('#navbar').removeClass("shrink");
    }
   });
 });

function takeToAdvancedSearch(){
  window.location.href="#";
  document.getElementById("SearchResults").style.display = "none";

  $("#simple-search").hide();
  $("#advanced_search").fadeIn();
}

function getFeaturedPlaylist(){
  $("#LoadingModal").modal({show: true});
  var playlist = new myPlaylists();

  $.ajax({
    url: 'https://api.spotify.com/v1/browse/featured-playlists',
    headers:{
      'Authorization': 'Bearer ' + access_token,
    },
    data:{
      limit:1,
      country:'US',
    },
    success: function (response) {
      playlist.name = response.playlists.items[0].name;
      var ownerID = response.playlists.items[0].owner.id;
      var playlistId = response.playlists.items[0].id;

      $.ajax({
        url: 'https://api.spotify.com/v1/users/'+ ownerID +'/playlists/'+ playlistId,
        headers:{
          'Authorization': 'Bearer ' + access_token,
        },
        success: function (r2) {

          for(var i = 0; i < r2.tracks.items.length;i++){
            playlist.songs.push(r2.tracks.items[i].track);
          }
          redirect(playlist);
        }
      });
    }
  });
}

var albums;
var songs;
var artists;
var currentJson;
var type;

function advancedSearch(){

  document.getElementById("SearchResults").style.display = "";
  document.getElementById("songs").innerHTML = "";
  document.getElementById("artists").innerHTML = "";

  var artist_query = document.getElementById("artist_query").value;
  var song_query = document.getElementById("song_query").value;

  var data;
  if(artist_query == "" && song_query == ""){
    //alert
    return;
  } else if(artist_query == ""){

    data = {
      q:song_query,
      type:'json',
      type:'track',
      limit: 8,
    };

  } else if(song_query == ""){

    data = {
      q:artist_query,
      type:'json',
      type:'artist',
      limit: 8,
    };

  } else {
    data = {
      q: 'artist:'+artist_query+' track:'+ song_query,
      type:'json',
      type:'track',
      limit: 8,
    };
  }

  $.ajax({
    url: 'https://api.spotify.com/v1/search',
    data: data,
    success: function (response) {
      songs = response.tracks;
      artists = response.artists;
      if(songs != null){
        getSong(songs);
      }
      if(artists != null){
        getArtist(artists);
      }
    },
    error: function (err) {
      console.log(err);
      $("#LoadingModal").modal({show: false});
    }

  });

  window.location.href = "#SearchResults";
}

function simpleSearch(){

  document.getElementById("SearchResults").style.display = "";
  document.getElementById("songs").innerHTML = "";
  document.getElementById("artists").innerHTML = "";

	var searchTerm = document.getElementById("simpleValue").value;
	if(searchTerm == ""){
		//alert
        return;
	}

  $.ajax({
    url: 'https://api.spotify.com/v1/search',
    data: {
      q: searchTerm,
      type: 'track,artist',
      limit: 4,
    },
    success: function (response) {
	songs = response.tracks;
	artists = response.artists;
	getSong(songs);
	getArtist(artists);
    }
  });

  document.getElementById("footer-below").style.display = "none";
  window.location.href = "#SearchResults";
}

function getSong(songs){
  for(var i = 0; i < songs.items.length; i++){
    var track = songs.items[i];
    document.getElementById("songs").innerHTML += '<div class="col-md-3 col-sm-6 col-xs-6"><div class="imageFrame"><figure><img width="200"  onclick="confirmPick(' + i + ',1)" src="' + track.album.images[1].url + '"></figure></div><div> ' + track.name + ' by ' + track.artists[0].name + '</div></div>';
  }
}

function getAlbum(albums){
  for(var i = 0; i < albums.items.length; i++){
    var album = albums.items[i];
    document.getElementById("albums").innerHTML += '<img width="150" onclick="confirmPick(' + i + ',2)" src="' + album.images[1].url + '"><div> ' + album.name + '</div>';
  }
}

function getArtist(artists){
  for(var i = 0; i < artists.items.length; i++){
    var artist = artists.items[i];
    document.getElementById("artists").innerHTML += '<div class="col-md-3 col-sm-6 col-xs-6"><div class="imageFrame"><figure><img width="200"  onclick="confirmPick(' + i + ',3)" src="' + artist.images[0].url + '"></figure></div><div> ' + artist.name  + '</div></div>';
  }
}

function confirmPick(i,t){
	if(t == 1) { // song
		var track = songs.items[i];
    type = 1;
		document.getElementById("modalBodyContainer").innerHTML = '<img width="150" src="' + track.album.images[1].url + '"><div> ' + track.name + ' by ' + track.artists[0].name + '</div>';
		currentJson = track;
	} else if(t == 2) { // album
    type = 2;
		var album = albums.items[i];
		document.getElementById("modalBodyContainer").innerHTML = '<img width="150" src="' + album.images[1].url + '"><div> ' + album.name + '</div>';
		currentJson = album;
	} else { // artist
    type= 3;
		var artist = artists.items[i];
		document.getElementById("modalBodyContainer").innerHTML = '<img width="150" src="' + artist.images[0].url + '"><div> ' + artist.name  + '</div>';
		currentJson = artist;
	}

	$("#confirmModal").modal({show: true});
}

var songSelection = 'song_hotttnesss';

var variety = (Math.random() * 0.6) + 0.3;
var adven = 0.1;

var seedArtists = "Muse";
var seedSongs = "SOOBPLQ135A6045666";
var playlistName ="My Playlist";

function generateByGenre(){
  genre = document.getElementById('dropdownMenu1').innerHTML.split(" ")[0].toLowerCase();
  if(genre == ""){
    //alert
  }
  else{
    if(genre == 'dance'){
      genre = 'dance pop';
    }
    else if(genre == "hip-hop"){
      genre = 'hip hop';
    }
    else if(genre == "alternative"){
      genre = "alternative pop rock";
    }
    type = 4;
    playlistName = genre;
    $("#GenreModal").modal({show: false});
    startSelection()
  }
}

function populateSearch(similartist) {
  if (similartist) {
    type = 3;
    currentJson = similartist;
    playlistName = "Playlist based on " + similartist.name;
  } else {
    var temp = document.getElementById("playlistName").value;
    if(temp != "") {
      playlistName = temp;
    }
  }

  if(type == 1) { // song
    $.ajax({
      url: 'http://developer.echonest.com/api/v4/song/search?',
      data: {
        api_key : 'JLCXGNF69UT38LAEE',
        format: 'json',
        title: currentJson.name,
        artist: currentJson.artists[0].name,
      },
      success: function (response) {
        seedArtists = currentJson.artists[0].name;
        seedSongs = response.response.songs[0].id;
        startSelection();
      }
    });
  } else if(type == 2) { // album
    startSelection();
  } else { // artist
    seedArtists = currentJson.name;
    startSelection();
  }
}

function startSelection() {
  $("#LoadingModal").modal({show: true});
  var dataByArtist = {
      "api_key" : 'JLCXGNF69UT38LAEE',
      "format": 'json',
      "type": 'artist-radio',
      "song_selection": songSelection,
      "variety": variety,
      "adventurousness": adven,
      "artist": seedArtists+"^1.5",
      artist_max_hotttnesss: maxHotness,
      artist_min_hotttnesss: minHotness,
      artist_start_year_after: minYear,
      artist_end_year_before: maxYear,
      max_danceability: maxDance,
      min_danceability: minDance,
      results: 25,
  };
  var dataBySong = {
      api_key : 'JLCXGNF69UT38LAEE',
      format: 'json',
      type: 'song-radio',
      song_selection: songSelection,
      variety: variety,
      adventurousness: adven,
      artist: seedArtists,
      song_id: seedSongs,
      artist_max_hotttnesss: maxHotness,
      artist_min_hotttnesss: minHotness,
      artist_start_year_after: minYear,
      artist_end_year_before: maxYear,
      max_danceability: maxDance,
      min_danceability: minDance,
      results: 25,
  };
  var dataByGenre = {
      api_key : 'JLCXGNF69UT38LAEE',
      format: 'json',
      type: 'genre-radio',
      song_selection: songSelection,
      variety: variety,
      adventurousness: adven,
      genre: genre,
      results: 25,
  };

  var data;
  if (type == 1) { // song
    data = dataBySong;
  } else if (type == 2) { // album
  } else if (type == 3) { // artist
    data = dataByArtist;
  } else { // genre
    data = dataByGenre;
  }

  $.ajax({
    url: 'http://developer.echonest.com/api/v4/playlist/static?bucket=id:spotify&bucket=tracks',
    data: data,
    success: function (response) {

      var i;
      var track;
      var s = response.response.songs;

      var foreign_ids = [];
      for(i = 0; i < s.length; i++){
        track = s[i];
        if(track.tracks[0] != undefined){
          foreign_ids.push(track.tracks[0].foreign_id);
        }
      }
      buildPlaylist(foreign_ids);
    },
    error: function (err) {
      console.log(err);
    }
  });
}

function buildPlaylist(foreign_ids) {
  var playlist = new myPlaylists();
  playlist.name = playlistName;
  if(type == 1){
    playlist.songs.push(currentJson);
  }
  var count = 0;
  for (var i = 0; i < foreign_ids.length; i++) {
    var words = foreign_ids[i].split(":");
    $.ajax({
      url: 'https://api.spotify.com/v1/tracks/'+words[2],
      data:{
        market: 'US',
      },
      success: function (response) {
        playlist.songs.push(response);
        count++;
        if (count === foreign_ids.length) {
          redirect(playlist);
        }
      },
      error: function() {
        count++;
        if (count === foreign_ids.length) {
          redirect(playlist);
        }
      }
    });
  }
}

function myPlaylists(){
  this.songs = [];
  this.name;
}

function redirect(playlist){
  var i = 0;
  //localStorage.removeItem("helloEchoPlaylists");
  var temp = localStorage.getItem("helloEchoPlaylists");
  if(localStorage.getItem("helloEchoPlaylists") != null && typeof JSON.parse(localStorage.getItem("helloEchoPlaylists")) === 'object'){
    var reset = JSON.parse(localStorage.getItem("helloEchoPlaylists"));
    reset.push(playlist);
    i = reset.length - 1;
    localStorage.setItem("helloEchoPlaylists", JSON.stringify(reset));
  }
  else{
    localStorage.setItem("helloEchoPlaylists", JSON.stringify([playlist]));
  }

  window.location.assign("playlist.html?i=" + i);
}
