var echonestKey = 'JLCXGNF69UT38LAEE';

var playlists; // used when updating names OR deleting playlist

var similartists; // used when seeding playlists based on similar artists

// Variables for updating track position
var time_stamps = []; // in ms
var track_durations = [];
var playing = -1;
var progress_interval = setInterval("updateProgressBar()", 10000);

var current_playlist;

var full_bio;

$(document).ready(function() {
    $('#results').on('click', '.btn-play', function() {
        playTrack(parseInt(this.id.substring(12)));
    });

    $('#results').on('click', '.btn-pause', function() {
        pauseTrack(parseInt(this.id.substring(13)));
    });

    playlists = JSON.parse(localStorage.getItem('helloEchoPlaylists'));
    var index = parseInt(gup('i'));
    if (index < playlists.length) {
        current_playlist = playlists[index];
        buildExtrasHTML();
    } else {
        current_playlist = {name: "n/a", songs: []};
    }

    buildPlaylistHTML(current_playlist, index);
});

function gup( name, url ) {
  if (!url) url = location.href;
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( url );
  return results == null ? null : results[1];
}

function playTrack(index) {
    pauseTrack(playing);
    $('#audio-' + index).trigger('play');
    $('#pause-button-' + index).show();
    $('#play-button-' + index).hide();
    playing = index;
    progress_interval = setInterval("updateProgressBar()", 50);
    buildExtrasHTML(current_playlist.songs[playing]);
}

function pauseTrack(index) {
    clearInterval(progress_interval)
    $('#audio-' + index).trigger('pause');
    $('#play-button-' + index).show();
    $('#pause-button-' + index).hide();
    playing = -1;
}

function playNextTrack() {
    if (playing + 1 < time_stamps.length) {
        playTrack(playing + 1);
    } else {
        pauseTrack(playing);
    }
}

function updateProgressBar() {
    if (playing != -1) {
        var duration = $('#audio-' + playing)[0].duration;
        if (duration != NaN) {
            track_durations[playing] = duration * 1000;
        } else {
            track_durations[playing] = 30000;
        }

        time_stamps[playing] = time_stamps[playing] + 50;
        var percent = 100.0 * time_stamps[playing] / track_durations[playing];

        if (percent > 100) {
            time_stamps[playing] = 0;
            $('#progress-bar-' + playing).html('<div id="progress-' + playing + '" class="progress-bar progress-bar-info" role="progressbar" style="width: 0%"></div>')
            playNextTrack();
        } else {
            $('#progress-bar-' + playing).html('<div id="progress-' + playing + '" class="progress-bar progress-bar-info" role="progressbar" style="width: ' + percent + '%"></div>')
        }
    }
}

function buildPlaylistHTML(playlist, index) {
    var songs = playlist.songs
    var html_string = '';
    time_stamps = [];
    track_durations = [];

    $.each(songs, function(index, item) {
        time_stamps[index] = 0;
        var html_start = '<div class="panel panel-default track-box"><div class="panel-body music-panel-items">';
        var html_end = '</div></div>';

        var box_art;
        if (item.album.images.length) {
            if (item.album.images[0].url) {
                box_art = '<div class="box-art"><img src="' + item.album.images[0].url + '" height=80px></div>';
            } else {
                box_art = '<div class="box-art"><img src="' + 'images/no_album_art.png' + '" height=80px></div>';
            }
        } else {
            box_art = '<div class="box-art"><img src="' + 'images/no_album_art.png' + '" height=80px></div>';
        }

        var track_name = '<div class="track_name"><b>' + item.name + '</b></div>';
        var artist_and_album = '<div class="artist_and_album">' + item.artists[0].name + ' - <i>' + item.album.name + '</i></div>';

        var track_info = '<div class="track_info">' + track_name + artist_and_album + '</div>';

        var audio = '<audio src="' + item.preview_url + '" id="audio-' + index + '"></audio>';

        var temp_audio = new Audio(item.preview_url);
        track_durations[index] = temp_audio.duration;

        var play_button = '<button id="play-button-' + index + '" type="button" class="btn btn-default btn-play"><span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>';
        var pause_button = '<button id="pause-button-' + index + '" type="button" class="btn btn-default btn-pause" style="display:none"><span class="glyphicon glyphicon-pause" aria-hidden="true"></span></button>';
        var progress_bar = '<div id="progress-bar-' + index + '" class="progress"><div id="progress-' + index + '" class="progress-bar progress-bar-info" role="progressbar" style="width: 0%"></div></div>';

        var media_control = '<div class="media-control">' + audio + play_button + pause_button + progress_bar + '</div>';

        var track_cell_right = '<div class="html_track_cell_right">' + track_info + media_control + '</div>';

        var new_html = html_start + box_art + track_cell_right + html_end;
        html_string = html_string + new_html;
    });

    if (html_string == '') {
        displayError('This playlist has no songs! Oops.');
        return
    }
    var playlist_title = '<h2>' + playlist.name;
    playlist_title += '<a class="glyph" href="javascript:editPlaylistModal(' + index + ')"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>';
    playlist_title += '<a class="glyph" href="javascript:deletePlaylistModal(' + index + ')"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></h2>';
    $("#playlist").html(playlist_title + html_string);
}

function displayError(errorMessage) {
    clearInterval(progress_interval);
    $("#playlist").html('');
    $('#error').html('<div class="alert alert-danger" role="alert">' + errorMessage + '</div>');
}

function buildExtrasHTML(song) {
    if (!song) {
        $("#extras-title").html("<h2>Extras!</h2>");
        $("#extras-bio").html("<p>Here's your playlist. We hope you like it and learn something new!</p>" +
                              "<p>You'll be able to read about and follow the musicians from right here if you'd like while you listen.</p>");
        return;
    }
    if (song.artists[0].uri) {
        $("#extras-title").html("<h2>" + song.artists[0].name + "</h2>");
        var artistURI = song.artists[0].uri
        var artistID = song.artists[0].id
        biographiesSearch(artistURI);
        twitterSearch(artistURI)
        buildSpotifollowHTML(artistURI);
        imageSearch(artistID);
        similartistSearch(artistURI);
    } else {
        alert('no artist id. everything is over!');
    }
}

function biographiesSearch(artistId){
    $.ajax({
        url: 'http://developer.echonest.com/api/v4/artist/biographies',
        data: {
            api_key: echonestKey,
            id: artistId,
            format: 'json',
            results: 1,
            start: 0,
            license: 'cc-by-sa'
        },
        success: function (response) {
            if (response.response.biographies && response.response.biographies[0] && response.response.biographies[0].text) {
                full_bio = response.response.biographies[0].text;
                if (full_bio.length <= 400) {
                    $("#extras-bio").html("<h4>Biography:</h4><p>" + full_bio + "...</p>");
                } else {
                    short_bio = full_bio.substring(0,400);
                    $("#extras-bio").html("<h4>Biography:</h4><p>" + short_bio + "...</p>" + "<button type=\"button\" class=\"btn btn-default btn-bio\" onclick=\"javascript:showFullBio();\">Show Full Bio</button>");
                }
            } else {
                $("#extras-bio").html('');
            }
        }
    });
}

function imageSearch(artistId, artist_name, i, cb){
    $.ajax({
        cache: false,
        type: "GET",
        url: "https://api.spotify.com/v1/artists/" + artistId,
        dataType: "json",
        success: function (response) {
            if (cb) {
                var url = ""
                if (response.images && response.images[0] && response.images[0].hasOwnProperty('url')) {
                    url = response.images[0].url;
                }
                cb(url, i, artist_name);
            } else {
                if (response.images && response.images[0] && response.images[0].hasOwnProperty('url')) {
                    $("#extras-image").html("<img class=\"bio-image\" src=\"" + response.images[0].url + "\"></a>");
                } else {
                    $("#extras-image").html("");
                }
            }
        }
    });
}

function twitterSearch(artistId){
    $.ajax({
        url: 'http://developer.echonest.com/api/v4/artist/twitter',
        data: {
            api_key: echonestKey,
            id: artistId,
            format: 'json'
        },
        success: function (response) {
            if (response.response && response.response.artist && response.response.artist.twitter) {
                var twit = "<p>Twitter: <a target=\"_blank\"href=\"http://twitter.com/" + response.response.artist.twitter + "\">@" + response.response.artist.twitter + "</a></p>";
                $("#extras-twitter").html(twit);
            } else {
                $("#extras-twitter").html("");
            }
        }
    });
}

function similartistSearch(artistId){
    $.ajax({
        url: 'http://developer.echonest.com/api/v4/artist/similar',
        data: {
            api_key: echonestKey,
            id: artistId,
            format: 'json',
            results: 8,
            start: 0,
            bucket:'id:spotify'
        },
        success: function (response) {
            var similartistHTML = "<h4>Generate a playlist based on a similar artist:</h4>";
            var similartistHTMLLeft = "<div class=\"similartist-column float-left\">";
            var similartistHTMLRight = "<div class=\"similartist-column float-right\">";
            imageCount = 0;
            similartists = response.response.artists;
            for (var i = 0; i < similartists.length; i++) {
                if (similartists[i] && similartists[i].foreign_ids) {
                    var foreign_id = similartists[i].foreign_ids[0].foreign_id;
                    var artist_name = similartists[i].name;
                    imageSearch(foreign_id.substring('spotify:artist:'.length), artist_name, i, function(url, i, name) {
                        imageCount++;
                        if (url !== "") {
                            if (imageCount%2) {
                                similartistHTMLLeft += "<div class=\"similar\"><a href=\"javascript:seedPlaylistModal(" + i + ")\"><img class=\"similartist-image\" src=\"" + url + "\"><p><b>" + name + "</b></p></a></div>";
                            } else {
                                similartistHTMLRight += "<div class=\"similar\"><a href=\"javascript:seedPlaylistModal(" + i + ")\"><img class=\"similartist-image\" src=\"" + url + "\"><p><b>" + name + "</b></p></a></div>";
                            }
                        }
                        if (imageCount === 8) {
                            similartistHTML += similartistHTMLLeft + "</div>" + similartistHTMLRight + "</div>";
                            $("#extras-similartists").html(similartistHTML);
                        }
                    });
                }
            }
        }
    });
}

function showFullBio() {
    $("#extras-bio").html("<h4>Biography:</h4><p>" + full_bio + "...</p>");
}

function buildSpotifollowHTML(artistID) {
    $('#extras-spotifollow').html('<iframe src="https://embed.spotify.com/follow/1/?uri=' + artistID + '&size=basic&theme=light" width="200" height="25" scrolling="no" frameborder="0" style="border:none; overflow:hidden;" allowtransparency="true"></iframe>');
}

function seedPlaylist(index) {
    populateSearch(similartists[index]);
}

function seedPlaylistModal(index) {
    var confirm_button = '<button class="btn btn-success" onclick="javascript:seedPlaylist(' + index + ')">Generate Playlist</button>';
    $('#seed-btn').html(confirm_button);

    var seed_playlist_message = 'Are you sure you want to generate a playlist based on the artist "' + similartists[index].name + '"?';
    $('#confirm-seed-playlist-modal-body').html(seed_playlist_message);

    $('#confirm-seed-playlist-modal').modal();
}


function editPlaylistModal(i) {
    var confirm_button = '<button class="btn btn-success" onclick="javascript:changePlaylistName(' + i + ')">Save</button>';
    $('#confirm-btn').html(confirm_button);

    var change_playlist_name_form = '<input id="edit-playlist-modal-text" type="text" class="form-control" placeholder="New Playlist Name" autofocus>';

    $('#edit-playlist-modal-title').html('Rename "' + playlists[i].name + '"');
    $('#edit-playlist-modal-body').html(change_playlist_name_form);
    $('#save-playlist-name-btn').attr('onclick', 'javascript:changePlaylistName("' + i + '")');

    $('#edit-playlist-modal').modal();
}

function changePlaylistName(i) {
    var newPlaylistName = $('#edit-playlist-modal-text').val();
    if (newPlaylistName === '') {
        $('#edit-playlist-modal').modal('hide');
        return;
    }
    playlists[i].name = newPlaylistName;
    localStorage.setItem("helloEchoPlaylists", JSON.stringify(playlists));
    $('#edit-playlist-modal').modal('hide');
    buildPlaylistHTML(playlists[i], i);
}

function deletePlaylistModal(i) {
    $('#delete-btn').html('');
    $('#edit-playlist-modal-title').html('Confirm Delete');
    $('#edit-playlist-modal-body').html('Are you sure you want to delete the playlist "' + playlists[i].name + '"?');
    $('#confirm-btn').html('<button id="save-playlist-name-btn" type="button" class="btn btn-danger" onclick="javascript:deletePlaylist(' + i + ')">Confirm Delete</button>');

    $('#edit-playlist-modal').modal();
}

function deletePlaylist(i) {
    playlists.splice(i, 1);
    localStorage.setItem("helloEchoPlaylists", JSON.stringify(playlists));
    $('#edit-playlist-modal').modal('hide');
    window.location.assign("myplaylists.html");
}
