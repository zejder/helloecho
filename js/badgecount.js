$(document).ready(function() {
    var playlists = JSON.parse(localStorage.getItem('helloEchoPlaylists'));

    if (!playlists || playlists.length === 0) {
      updateBadgeCount(0);
    } else {
      updateBadgeCount(playlists.length);
    }
});

function updateBadgeCount(count) {
    $('#playlist-count').text(count);
}
